# Docker commands
## Listar containers
Apenas os que estão rodando
```sh
docker ps
```
Todos containers
```sh
docker ps -a
```
## Rodar um container
##### docker container run [OPTIONS] IMAGE [COMMAND] [ARG...]
Comando:
```sh
docker container run -d --name <DEFINA_UM_NOME> --restart=always -p <PORTA_HOST>:<PORTA_CONTAINER> <NOME_DA_IMAGEM>
```
Ex.:
```sh
docker container run -d --name redis --restart=always -p 6379:6379 redis
```
option | description
--- | ---
`-d` | **Roda container em background**
`--name` | **Define noma ao container**
`--restart`| **Para restartar o container de acordo com políticas**
`-p` | **Publica porta do container para o host**
## Inspecionar um container
##### docker inspect [OPTIONS] NAME|ID [NAME|ID...]
Comando:
```sh
docker inspect <CONTAINER ID>
```
## Build de uma imagem
##### docker build [OPTIONS] PATH | URL | -
Comando (considerando que o arquivo Dockerfile está dentro da pasta "docker" ):
```sh
docker build -t <NOME_DA_IMAGE> -f docker/Dockerfile .
```
Comando para rodar a imagem gerada:
```sh
docker run -d --name <DEFINA_UM_NOME> -p <PORTA_HOST>:<PORTA_CONTAINER> <NOME_DA_IMAGEM>
```
## Docs
https://docs.docker.com/engine/reference/commandline/run/#/restart-policies

https://docs.docker.com/config/containers/container-networking/
