require('dotenv').config()
const express = require('express')
const log = require('ololog').configure({ tag: true })
const app = express()
const { uuid } = require('uuidv4')
const PORT = process.env.SERVER_PORT || 3000

app.get('/', function (req, res) {
  const id = uuid()
  log.info(id)
  res.json({
    id
  })
})

app.listen(PORT, function () {
  log.info(`app listening on port ${PORT}`)
})
